codes = true
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'not_equals',
    'array_equals',
    'is_string',
    'is_table',
    'matches',
    'require_ok',
}
files['test/06-external.lua'].ignore = { '111/CodeGen' }
