#!/usr/bin/env lua

require 'Test.Assertion'

plan(8)

if not require_ok 'CodeGen' then
    BAIL_OUT "no lib"
end

local m = require 'CodeGen'
is_table( m )
equals( m, package.loaded.CodeGen )

equals( m._NAME, 'CodeGen', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'template engine', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

