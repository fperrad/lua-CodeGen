#!/usr/bin/env lua

local CodeGen = require 'CodeGen'

require 'Test.Assertion'

plan(8)

local tmpl = CodeGen()
is_table( tmpl, "new CodeGen" )
tmpl.a = 'some text'
equals( tmpl 'a', 'some text', "eval 'a'" )

tmpl = CodeGen{
    pi = 3.14159,
    str = 'some text',
}
is_table( tmpl, "new CodeGen" )
tmpl.pi = 3.14
equals( tmpl 'str', 'some text', "eval 'str'" )
equals( tmpl 'pi', '3.14', "eval 'pi'" )
not_equals( tmpl 'pi', 3.14 )
equals( tmpl 'unk', '', "unknown gives an empty string" )

equals( tostring(tmpl), 'CodeGen', "__tostring" )
