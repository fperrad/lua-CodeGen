#!/usr/bin/env lua

local CodeGen = require 'CodeGen'

require 'Test.Assertion'

plan(5)

local tmpl = CodeGen{
    outer = [[
begin
    ${inner()}
end
]],
    inner = [[print("${hello}");]],
    hello = "Hello, world!",
}
equals( tmpl 'outer', [[
begin
    print("Hello, world!");
end
]] , "" )

tmpl.inner = 3.14
local res, msg = tmpl 'outer'
equals( res, [[
begin
    ${inner()}
end
]] , "not a template" )
equals( msg, "outer:2: inner is not a template" )

tmpl = CodeGen{
    top = [[
${outer()}
]],
    outer = [[
begin
    ${inner()}
end
]],
    inner = [[print("${outer()}");]],
}
res, msg = tmpl 'top'
equals( res, [[
begin
    print("${outer()}");
end
]], "cyclic call" )
equals( msg, "inner:1: cyclic call of outer" )
