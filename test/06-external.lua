#!/usr/bin/env lua

CodeGen = require 'CodeGen'

require 'Test.Assertion'

plan(1)

local tmpl = dofile 'test/tmpl.tmpl'
tmpl.data = {
    { name = 'key1', value = 1 },
    { name = 'key2', value = 2 },
    { name = 'key3', value = 3 },
}
equals( tmpl 'top', [[
begin
        print("key1 = 1");
        print("key2 = 2");
        print("key3 = 3");
end
]] , "external" )
